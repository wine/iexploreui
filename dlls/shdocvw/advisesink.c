#include "shdocvw.h"
#include "wine/debug.h"
#include "mshtml.h"

WINE_DEFAULT_DEBUG_CHANNEL(shdocvw);

#define ADVISESINK_THIS(iface) DEFINE_THIS(AdviseSink, AdviseSink, iface)

static ULONG WINAPI AdviseSink_AddRef(IAdviseSink* iface)
{
    AdviseSink* This = ADVISESINK_THIS(iface);
    LONG ref = InterlockedIncrement(&This->ref);

    TRACE("(%p) ref = %d\n", iface, ref);

    return ref;
}

static HRESULT WINAPI AdviseSink_QueryInterface(IAdviseSink* iface, REFIID riid, void** ppvoid)
{
    TRACE("(%p)->(%s,%p)\n", iface, debugstr_guid(riid), ppvoid);

    *ppvoid = NULL;
    if(IsEqualGUID(&IID_IAdviseSink, riid))
    {
        *ppvoid = iface;
        AdviseSink_AddRef(iface);
        return S_OK;
    }

    return E_NOINTERFACE;
}

static ULONG WINAPI AdviseSink_Release(IAdviseSink* iface)
{
    AdviseSink* This = ADVISESINK_THIS(iface);
    LONG ref = InterlockedDecrement(&This->ref);;

    TRACE("(%p) ref = %d\n", iface, ref);

    if(!ref)
    {
        if(This->doc_host)
            This->doc_host->advise_sink = NULL;
        heap_free(This);
    }

    return ref;
}

static void WINAPI AdviseSink_OnDataChange(IAdviseSink* iface, FORMATETC* pFormatetc, STGMEDIUM* pStgmed)
{
    FIXME("(%p)->(%p,%p): stub\n", iface, pFormatetc, pStgmed);
}

static void WINAPI AdviseSInk_OnViewChange(IAdviseSink* iface, DWORD dwAspect, LONG lindex)
{
    AdviseSink* This = ADVISESINK_THIS(iface);
    IHTMLDocument2* htmldoc2;
    HRESULT hres;

    TRACE("(%p)->(%x,%d)\n", iface, dwAspect, lindex);

    hres = IUnknown_QueryInterface(This->doc_host->document, &IID_IHTMLDocument2, (void**)&htmldoc2);

    if(SUCCEEDED(hres))
    {
        BSTR url;
        BSTR title;

        if(SUCCEEDED(IHTMLDocument2_get_URL(htmldoc2, &url)))
            SendMessageW(This->doc_host->frame_hwnd, WM_UPDATEADDRBAR, 0, (LPARAM)&url);

        if(SUCCEEDED(IHTMLDocument2_get_title(htmldoc2, &title)))
            SendMessageW(This->doc_host->frame_hwnd, WM_UPDATETITLE, 0, (LPARAM)title);

        IHTMLDocument2_Release(htmldoc2);
    }
}

static void WINAPI AdviseSink_OnRename(IAdviseSink* iface, IMoniker* pmk)
{
    FIXME("(%p)->(%p): stub\n", iface, pmk);
}

static void WINAPI AdviseSink_OnSave(IAdviseSink* iface)
{
    FIXME("(%p): stub\n", iface);
}

static void WINAPI AdviseSink_OnClose(IAdviseSink* iface)
{
    FIXME("(%p): stub\n", iface);
}

static const IAdviseSinkVtbl AdviseSinkVtbl = {
    AdviseSink_QueryInterface,
    AdviseSink_AddRef,
    AdviseSink_Release,
    AdviseSink_OnDataChange,
    AdviseSInk_OnViewChange,
    AdviseSink_OnRename,
    AdviseSink_OnSave,
    AdviseSink_OnClose
};

void DocHost_AdviseSink_Init(AdviseSink** This)
{
    *This = heap_alloc(sizeof(AdviseSink));
    (*This)->lpAdviseSinkVtbl = &AdviseSinkVtbl;
    (*This)->ref = 1;
}
